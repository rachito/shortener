import random
import string

from django.conf import settings
from django.db import models


class Link(models.Model):
    url = models.URLField(max_length=1000)
    short_id = models.CharField(max_length=20, unique=True, default='')
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f"{self.url} > {self.shorten_url}"

    @staticmethod
    def get_short_id():
        n = 6
        return ''.join(random.choice(string.ascii_letters + string.digits) for i in range(n))

    @property
    def shorten_url(self):
        return settings.SHORTENER_BASE_URL + self.short_id

    def save(self, *args, **kwargs):
        self.short_id = self.get_short_id()
        super(Link, self).save(*args, **kwargs)
