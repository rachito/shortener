from django.shortcuts import redirect, get_object_or_404

from .models import Link


def link_redirect(request, short_id):
    link = get_object_or_404(Link, short_id=short_id)
    return redirect(link.url)
