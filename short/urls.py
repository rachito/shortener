from .apiviews import LinkViewSet
from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register('links', LinkViewSet, base_name='shortener')

urlpatterns = []
urlpatterns += router.urls
