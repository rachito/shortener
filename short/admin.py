from django.contrib import admin

from .models import Link


class LinkAdmin(admin.ModelAdmin):
    exclude = ('short_id',)


admin.site.register(Link, LinkAdmin)
