## Instalación

1. Clona el repositorio: `git clone git@bitbucket.org:rachito/shortener.git`
2. La rama a usar es: ***master***
3. Crea un entorno virtual con virtualenv y activalo con los comando correspondientes (Paso opcional)
4. Instala las dependencias del proyecto con  `pip install -r requirements.txt`
5. Una vez instaladas todas las dependencias ejecuta el comando `python manage.py runserver`
6. La aplicación quedará corriendo en: http://127.0.0.1:8000

**Nota**: Para fines prácticos de evaluación se versionaron: el archivo settings.py y el archivo de base de datos de sqlite.

## Urls de proyecto
**API:** http://127.0.0.1:8000/api/
**API Docs:** http://127.0.0.1:8000/docs
**Admin:** http://127.0.0.1:8000/admin

### Usuarios
**administrador**: 
*usuario*: developers, *contraseña*: holamundo

## Manual de uso
La api funciona como cualquier otra api RestFul, el recurso "/links" soporta los verbos mas comunes (Checar Documentación).

Para acortar un link solo en necesario hacer un POST "/links" con la url correspondiente tal y como se indica en la documentación.